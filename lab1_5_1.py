################################################### Connecting to AWS
import boto3

import json
################################################### Create random name for things
import random
import string

################################################### Parameters for Thing
thingArn = ''
thingId = ''
defaultPolicyName = 'My_Iot_Policy'
###################################################

def createThing(thingName):
  global thingClient
  thingResponse = thingClient.create_thing(
      thingName = thingName
  )
  data = json.loads(json.dumps(thingResponse, sort_keys=False, indent=4))
  for element in data:
    if element == 'thingArn':
        thingArn = data['thingArn']
    elif element == 'thingId':
        thingId = data['thingId']
        createCertificate(thingName)

def createCertificate(thingName):
    global thingClient
    certResponse = thingClient.create_keys_and_certificate(
            setAsActive = True
    )
    data = json.loads(json.dumps(certResponse, sort_keys=False, indent=4))
    for element in data:
            if element == 'certificateArn':
                    certificateArn = data['certificateArn']
            elif element == 'keyPair':
                    PublicKey = data['keyPair']['PublicKey']
                    PrivateKey = data['keyPair']['PrivateKey']
            elif element == 'certificatePem':
                    certificatePem = data['certificatePem']
            elif element == 'certificateId':
                    certificateId = data['certificateId']

    with open(thingName + 'public.key', 'w') as outfile:
            outfile.write(PublicKey)
    with open(thingName + 'private.key', 'w') as outfile:
            outfile.write(PrivateKey)
    with open(thingName + 'cert.pem', 'w') as outfile:
            outfile.write(certificatePem)

    response = thingClient.attach_policy(
            policyName = defaultPolicyName,
            target = certificateArn
    )
    response = thingClient.attach_thing_principal(
            thingName = thingName,
            principal = certificateArn
    )


thingClient = boto3.client('iot', aws_access_key_id='AKIA27SQDKPVWAQJ2U7T', aws_secret_access_key='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',
              region_name='us-east-2')

thingGroupName = 'MyIotThingGroup'
thingGroupArn = 'arn:aws:iot:us-east-2:755008492523:thinggroup/MyIotThingGroup'

for x in range(9):
    thingName = f'device_{x}'
    createThing(thingName)
    thingClient.add_thing_to_thing_group(
        thingGroupName= thingGroupName,
        thingGroupArn= thingGroupArn,
        thingName= thingName,
        thingArn= thingArn
    )